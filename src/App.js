import React from "react";
import { Link, Route, Routes } from "react-router-dom";
import LocalTodo from "./Page/LocalTodo";
import ApiTodo from "./Page/ApiTodo";
import ApiTodoUser from "./Page/ApiTodoUser";

const Home = () => {
  return (
    <div className="bg-emerald-200 w-full h-[95vh] flex justify-center items-center">
      <h1 className="px-5 text-gray-500 font-bold text-center">
        Learn React-Router-Dom V6, React Todo CRUD and React Todo API CRUD &
        Redux-Toolkit.
      </h1>
    </div>
  );
};

const NotFound = () => {
  return (
    <div className="bg-gray-200 w-full h-[95vh] flex justify-center items-center">
      <h1 className="px-5 text-gray-500 font-bold">404 Not Found!</h1>
    </div>
  );
};

function App() {
  return (
    <>
      <nav className="h-12 flex justify-center bg-blue-200">
        <ul className="flex justify-center items-center gap-4">
          <li className="font-bold">
            <Link to="/">Home</Link>
          </li>
          <li className="font-bold">
            <Link to="/todo">LocalTodo</Link>
          </li>
          <li className="font-bold">
            <Link to="/api-todo">APITodo</Link>
          </li>
        </ul>
      </nav>

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/todo" element={<LocalTodo />} />
        <Route path="/api-todo" element={<ApiTodo />}>
          <Route path=":id" element={<ApiTodoUser />} />
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
}

export default App;
