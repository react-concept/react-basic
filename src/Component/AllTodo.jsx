import React from "react";
import Todo from "./Todo";

const AllTodo = ({ todoData, changeStatus, deleteTodo, editTodo }) => {
  return (
    <div className="my-5 px-5 w-full lg:w-[75%]">
      {todoData?.length ? (
        <div>
          {todoData?.map((todo) => (
            <Todo
              key={todo?.id}
              todo={todo}
              changeStatus={changeStatus}
              deleteTodo={deleteTodo}
              editTodo={editTodo}
            />
          ))}
        </div>
      ) : (
        <p>No data In Todo List</p>
      )}
    </div>
  );
};

export default AllTodo;
