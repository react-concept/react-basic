import React from "react";

const Todo = ({ todo, changeStatus, deleteTodo, editTodo }) => {
  return (
    <div className="w-full h-16 grid grid-cols-10 gap-2 py-2">
      <input
        className="h-6 w-6 col-span-1"
        type="checkbox"
        name={todo?.title}
        id={todo?.id}
        checked={todo?.completed}
        onChange={() => changeStatus(todo?.id)}
      />
      <div className="ww-full h-full overflow-x-hidden overflow-y-auto text-xl col-span-6 lg:col-span-7">
        {todo?.title}
      </div>
      <button
        className="border-2 col-span-2 lg:col-span-1 border-gray-500 rounded-md px-2 bg-slate-300"
        onClick={() => editTodo(todo.id)}
      >
        Edit
      </button>
      <button
        className="border-2 col-span-1 border-gray-500 rounded-md px-2 bg-slate-300"
        onClick={() => deleteTodo(todo?.id)}
      >
        X
      </button>
    </div>
  );
};

export default Todo;
