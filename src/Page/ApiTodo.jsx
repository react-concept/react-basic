import React, { useEffect, useState } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { setActiveUser } from "../Slices/activeUserSlices";
import { useDispatch, useSelector } from "react-redux";

const ApiTodo = () => {
  const [users, setUsers] = useState([]);
  const navigate = useNavigate();

  const activeUser = useSelector((state) => state.activeUser.value);

  const dispatch = useDispatch();

  useEffect(() => {
    fetchAllUser();
  }, []);

  const fetchAllUser = async () => {
    try {
      await fetch("https://jsonplaceholder.typicode.com/users")
        .then((response) => response.json())
        .then((json) => setUsers(json));
    } catch (error) {
      console.log("🚀 ~ file: ApiTodo.jsx:13 ~ fetchAllUser ~ error:", error);
    }
  };

  const handleChange = (e) => {
    if (e.target.value === "select") {
      navigate(`/api-todo`);
      dispatch(setActiveUser(null));
      return;
    }
    navigate(`/api-todo/${parseInt(e.target.value)}`);
  };

  if (!users.length) {
    return (
      <div className="bg-gray-200 w-full h-[95vh] flex justify-center items-center">
        <h1>Loading...</h1>
      </div>
    );
  }

  return (
    <div className="w-full md:w-[75%] my-5 px-5">
      <div className="flex gap-5">
        <label className="font-semibold" htmlFor="selectUser">
          Select User :
        </label>
        <select
          className="h-8 rounded-full px-5"
          id="selectUser"
          onChange={handleChange}
        >
          <option value="select">{activeUser?.name || "Select"}</option>
          {users?.map((user) => (
            <option key={user.id} value={user.id}>
              {user.id}. {user.name}
            </option>
          ))}
        </select>
      </div>
      <hr className="my-5" />
      <Outlet />
    </div>
  );
};

export default ApiTodo;
