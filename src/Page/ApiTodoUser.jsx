import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import AllTodo from "../Component/AllTodo";
import { useDispatch } from "react-redux";
import { setActiveUser } from "../Slices/activeUserSlices";

const ApiTodoUser = () => {
  const [user, setUser] = useState({ detail: null, todo: null });
  const [loading, setLoading] = useState(false);

  const { id } = useParams();

  const [editId, setEditId] = useState(null);
  const inputRef = useRef(null);

  const dispatch = useDispatch();

  useEffect(() => {
    if (id) {
      setLoading(true);
      fetchData();
    }
  }, [id]);

  const fetchData = async () => {
    try {
      const userDetail = await axios(
        `https://jsonplaceholder.typicode.com/users/${id}`
      );
      const userTodo = await axios(
        `https://jsonplaceholder.typicode.com/users/${id}/todos`
      );

      setUser({
        detail: userDetail.data,
        todo: userTodo.data,
      });
      setLoading(false);
      dispatch(setActiveUser(userDetail.data));
    } catch (error) {
      console.log("🚀 ~ file: ApiTodoUser.jsx:19 ~ fetchData ~ error:", error);
    }
  };

  if (!id || loading) {
    return (
      <div className="bg-gray-200 w-full h-[95vh] flex justify-center items-center">
        <h1>Loading...</h1>
      </div>
    );
  }

  const addTodo = async () => {
    let input = inputRef.current.value;
    if (!input || input?.trim() === "") {
      inputRef.current.value = "";
      return;
    }

    try {
      await fetch(`https://jsonplaceholder.typicode.com/users/${id}/todos`, {
        method: "POST",
        body: JSON.stringify({
          title: input.trim(),
          completed: false,
        }),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json; charset=UTF-8",
        },
      })
        .then((res) => {
          if (res.status !== 201) {
            return;
          } else {
            return res.json();
          }
        })
        .then((data) => {
          setUser({
            ...user,
            todo: [...user.todo, data],
          });
          inputRef.current.value = "";
        });
    } catch (error) {
      console.log("🚀 ~ file: ApiTodoUser.jsx:59 ~ addTodo ~ error:", error);
    }
  };

  const changeStatus = async (todoId) => {
    let todo = user?.todo?.find((todo) => todo.id === parseInt(todoId));
    try {
      const res = await axios.put(
        `https://jsonplaceholder.typicode.com/todos/${todoId}`,
        {
          completed: !todo?.completed,
        }
      );
      if (res?.status !== 200) throw res.error;
      else {
        setUser({
          ...user,
          todo: user?.todo?.map((todo) => {
            if (todo.id === parseInt(todoId)) {
              return {
                ...todo,
                completed: res?.data?.completed,
              };
            }
            return todo;
          }),
        });
      }
    } catch (error) {
      console.log(
        "🚀 ~ file: ApiTodoUser.jsx:92 ~ changeStatus ~ error:",
        error
      );
    }
  };

  const deleteTodo = async (todoId) => {
    if (parseInt(todoId) === parseInt(editId)) return;
    try {
      const res = await axios.delete(
        `https://jsonplaceholder.typicode.com/todos/${todoId}`
      );
      if (res?.status !== 200) throw res.error;
      else {
        setUser({
          ...user,
          todo: user?.todo?.filter((todo) => todo.id !== parseInt(todoId)),
        });
      }
    } catch (error) {
      console.log(
        "🚀 ~ file: ApiTodoUser.jsx:92 ~ changeStatus ~ error:",
        error
      );
    }
  };

  const editTodo = (todoId) => {
    inputRef.current.value = user?.todo?.find(
      (todo) => todo.id === parseInt(todoId)
    )?.title;
    setEditId(todoId);
  };

  const saveEditTodo = async () => {
    let input = inputRef.current.value;
    if (!input || input?.trim() === "") {
      inputRef.current.value = "";
      setEditId(null);
      return;
    }

    try {
      const res = await axios.put(
        `https://jsonplaceholder.typicode.com/todos/${editId}`,
        {
          title: input?.trim(),
        }
      );
      if (res?.status !== 200) throw res.error;
      else {
        setUser({
          ...user,
          todo: user?.todo?.map((todo) => {
            if (todo.id === parseInt(editId)) {
              return {
                ...todo,
                title: res?.data?.title,
              };
            }
            return todo;
          }),
        });
        inputRef.current.value = "";
      }
    } catch (error) {
      console.log(
        "🚀 ~ file: ApiTodoUser.jsx:92 ~ changeStatus ~ error:",
        error
      );
    }
  };

  return (
    <>
      <div>ApiTodoUser : {id}</div>
      <div className="w-full h-32 bg-blue-300 rounded-md flex flex-col justify-center items-center">
        <h1>Name : {user?.detail?.name}</h1>
        <h1>Email : {user?.detail?.email}</h1>
        <h1>Phone : {user?.detail?.phone}</h1>
        <h1>Website : {user?.detail?.website}</h1>
      </div>
      <AllTodo
        todoData={user?.todo}
        changeStatus={changeStatus}
        deleteTodo={deleteTodo}
        editTodo={editTodo}
      />
      <div className="w-full md:w-[75%] my-5 px-5 grid grid-cols-3 gap-4">
        <input
          type="text"
          placeholder="Enter Todo Name"
          className="h-10 col-span-2 bg-slate-300 border-2 border-gray-500 rounded-full p-2"
          ref={inputRef}
        />
        {!editId ? (
          <button
            className="p-2 h-10 bg-slate-400 border-2 border-gray-500 rounded-lg text-white"
            onClick={addTodo}
          >
            Add Todo
          </button>
        ) : (
          <button
            className="p-2 h-10 bg-slate-400 border-2 border-gray-500 rounded-lg text-white"
            onClick={saveEditTodo}
          >
            Edit Todo
          </button>
        )}
      </div>
      <p className="ml-5 mb-2">
        {user?.todo?.length
          ? user?.todo?.reduce((acc, cur) => {
              if (cur.completed) acc++;
              return acc;
            }, 0)
          : 0}{" "}
        Completed Todo
      </p>
    </>
  );
};

export default ApiTodoUser;
