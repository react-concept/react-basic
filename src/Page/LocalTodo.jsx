import React, { useEffect, useRef, useState } from "react";
import AllTodo from "../Component/AllTodo";

const LOCAL_STORE_NAME = "LocalTodo";

const LocalTodo = () => {
  const [todoData, setTodoData] = useState([]);
  const [editId, setEditId] = useState(null);
  const inputRef = useRef(null);

  useEffect(() => {
    const todo = JSON.parse(localStorage.getItem(LOCAL_STORE_NAME));
    if (todo?.length) {
      setTodoData([...todo]);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem(LOCAL_STORE_NAME, JSON.stringify(todoData));
  }, [todoData]);

  const addTodo = () => {
    let input = inputRef.current.value;
    if (!input || input?.trim() === "") {
      inputRef.current.value = "";
      return;
    }

    setTodoData((prev) => {
      return [
        ...prev,
        {
          id: Math.random().toString(36).substring(5),
          title: input?.trim(),
          completed: false,
        },
      ];
    });
    inputRef.current.value = "";
  };

  const changeStatus = (id) => {
    setTodoData(
      todoData.map((todo) => {
        if (todo?.id === id) {
          return { ...todo, completed: !todo.completed };
        }
        return todo;
      })
    );
  };

  const removeCompleted = () => {
    if (editId && todoData.find((todo) => todo.id === editId)?.completed)
      return;
    setTodoData(todoData.filter((todo) => !todo.completed));
  };

  const deleteTodo = (id) => {
    if (editId === id) return;
    setTodoData(todoData.filter((todo) => !(todo.id === id)));
  };

  const editTodo = (id) => {
    inputRef.current.value = todoData.find((todo) => todo.id === id)?.title;
    setEditId(id);
  };

  const saveEditTodo = () => {
    let input = inputRef.current.value;
    if (!input || input?.trim() === "") {
      inputRef.current.value = "";
      setEditId(null);
      return;
    }

    setTodoData(
      todoData?.map((todo) => {
        if (todo.id === editId) {
          return { ...todo, title: input?.trim() };
        }
        return todo;
      })
    );
    inputRef.current.value = "";
    setEditId(null);
  };

  return (
    <>
      <AllTodo
        todoData={todoData}
        changeStatus={changeStatus}
        deleteTodo={deleteTodo}
        editTodo={editTodo}
      />
      <div className="w-full lg:w-[75%] my-5 px-5 grid grid-cols-3 gap-4">
        <input
          type="text"
          placeholder="Enter Todo Name"
          className="h-10 col-span-2 bg-slate-300 border-2 border-gray-500 rounded-full p-2"
          ref={inputRef}
        />
        {!editId ? (
          <button
            className="p-2 h-10 bg-slate-400 border-2 border-gray-500 rounded-lg text-white"
            onClick={addTodo}
          >
            Add Todo
          </button>
        ) : (
          <button
            className="p-2 h-10 bg-slate-400 border-2 border-gray-500 rounded-lg text-white"
            onClick={saveEditTodo}
          >
            Edit Todo
          </button>
        )}
      </div>
      <p className="ml-5 mb-2">
        {todoData?.reduce((acc, cur) => {
          if (cur.completed) acc++;
          return acc;
        }, 0)}{" "}
        Completed Todo
      </p>
      <button
        className="ml-5 h-10 p-2 text-white bg-red-400 border-2 border-gray-500 rounded-lg"
        onClick={removeCompleted}
      >
        Clear Complete Todo
      </button>
    </>
  );
};

export default LocalTodo;
