import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: null,
};

export const activeUserSlice = createSlice({
  name: "activeUser",
  initialState,
  reducers: {
    setActiveUser: (state, action) => {
      state.value = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setActiveUser } = activeUserSlice.actions;

export default activeUserSlice.reducer;
