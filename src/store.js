import { configureStore } from "@reduxjs/toolkit";
import activeUserReducer from "../src/Slices/activeUserSlices";

export const store = configureStore({
  reducer: {
    activeUser: activeUserReducer,
  },
});
